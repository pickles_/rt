# RT project

Use `make all` or `make RT` to install all dependencies and compile project.
Use `make update` to recompile only project files and libft.
### **!!! Nota bene**
Do not use `make re`, `make fclean` to update project.  
This commands will delete all libraries.
If you do not want to wait long, do not use it.
