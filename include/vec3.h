/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec3.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:15:07 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:15:07 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RTV1_VEC3_H
# define RTV1_VEC3_H

# ifdef __APPLE__
#  include <OpenCL/opencl.h>
# else
#  define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#  include <CL/cl.h>
# endif

typedef cl_double3	t_vec3;

/*
** vec3_products.c
*/
double	vec3_dot(t_vec3 v1, t_vec3 v2);
t_vec3	vec3_scale(t_vec3 v, double s);
t_vec3	vec3_cross(t_vec3 v1, t_vec3 v2);
t_vec3	vec3_product(t_vec3 v1, t_vec3 v2);
double	vec3_cos(t_vec3 v1, t_vec3 v2);

/*
** vec3.c
*/
t_vec3	vec3_new(double x, double y, double z);
double	vec3_length2(t_vec3 v);
double	vec3_length(t_vec3 v);
t_vec3	vec3_normalized(t_vec3 v);
double	vec3_normalize(t_vec3 *v);

/*
** vec3_other.c
*/
t_vec3	vec3_sum(t_vec3 v1, t_vec3 v2);
t_vec3	vec3_sub(t_vec3 v1, t_vec3 v2);
t_vec3	vec3_div(t_vec3 v1, t_vec3 v2);
t_vec3	vec3_add_num(t_vec3 v1, double v);
t_vec3	vec3_sub_num(t_vec3 v1, double v);

#endif
