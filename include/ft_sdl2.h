/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sdl2.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/12 17:34:38 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/12 17:34:39 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RTV1_FT_SDL2_H
# define RTV1_FT_SDL2_H

# include <stdlib.h>
# include <SDL2/SDL.h>

# define AMASK 0xff000000
# define RMASK 0x00ff0000
# define GMASK 0x0000ff00
# define BMASK 0x000000ff

typedef struct		s_win
{
	SDL_Window		*window;
	SDL_Renderer	*renderer;
	struct s_win	*next;
}					t_win;

typedef struct		s_sdl2
{
	SDL_DisplayMode	display_mode;
	t_win			*win;
}					t_sdl2;

typedef struct		s_img
{
	SDL_Texture		*texture;
	SDL_Surface		*surface;
	Uint32			*pixels;
	int				bpp;
	int				width;
	int				height;
}					t_img;

/*
** sdl2_image.c
*/
void				*sdl2_destroy_img(t_img **img);
t_img				*sdl2_new_img(t_win *win, int w, int h);
void				sdl2_img_put_pixel(t_img *img, int x, int y, Uint32 color);
Uint32				sdl2_img_get_pixel(t_img *img, int x, int y);

/*
** sdl2_init.c
*/
t_sdl2				*sdl2_init();
void				*sdl2_destroy(t_sdl2 **sdl2);

/*
** sdl2_main_loop.c
*/
void				sdl2_main_loop(t_sdl2 *sdl2);

/*
** sdl2_put_image_to_window.c
*/
void				sdl2_put_image_to_window(t_win *win, t_img *img,
											int x, int y);
/*
** sdl2_window.c
*/
t_win				*sdl2_new_win(t_sdl2 *sdl2, const char *title,
									int w, int h);
void				*sdl2_delete_window(t_win **win);
void				*sdl2_destroy_window(t_sdl2 *sdl2, t_win *win);

#endif
