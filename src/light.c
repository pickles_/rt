/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   light.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:14:17 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:14:17 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <include/rtv1.h>

void		light_delete_in_list(void *light, size_t unused)
{
	(void)unused;
	ft_memdel(&light);
}

t_light		*light_init(t_point3 pos, int color)
{
	t_light		*light;

	if (!(light = ft_memalloc(sizeof(t_light))))
		return (NULL);
	light->color = color;
	light->pos = pos;
	ft_bzero(light->ia.s, sizeof(cl_double) * 4);
	ft_bzero(light->id.s, sizeof(cl_double) * 4);
	ft_bzero(light->is.s, sizeof(cl_double) * 4);
	light->pow = 0.0;
	return (light);
}
