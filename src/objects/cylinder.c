/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cylinder.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:14:11 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:14:11 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <include/rtv1.h>

#define CYLINDER ((t_cylinder *)data)

int			cylinder_get_color(t_obj3d *obj3d)
{
	if (!(obj3d && obj3d->material))
		return (0);
	return (obj3d->material->color | AMASK);
}

t_vec3		cylinder_get_normal(void *data, t_ray *ray, double t)
{
	t_vec3	p;
	t_vec3	rp;
	t_vec3	n;

	p = ray_comp_point(ray, t);
	rp = vec3_sub(CYLINDER->pos, p);
	n = vec3_scale(CYLINDER->dir, vec3_dot(CYLINDER->dir, rp));
	n = vec3_sum(n, vec3_sub(p, CYLINDER->pos));
	return (n);
}

int			cylinder_intersect(void *data, t_ray *ray, double *t,
								t_vec3 *normal)
{
	t_vec3	pos;
	t_vec3	u;
	t_vec3	v;
	t_vec3	abc;

	pos = vec3_sub(CYLINDER->pos, ray->orig);
	u = vec3_sub(ray->dir, vec3_scale(CYLINDER->dir,
										vec3_dot(ray->dir, CYLINDER->dir)));
	v = vec3_sub(pos, vec3_scale(CYLINDER->dir, vec3_dot(pos, CYLINDER->dir)));
	abc.s0 = vec3_dot(u, u);
	abc.s1 = 2 * vec3_dot(v, u);
	abc.s2 = vec3_dot(v, v) - CYLINDER->radius * CYLINDER->radius;
	if (!solve_quadratic(abc.s0, abc.s1, abc.s2, t))
		return (0);
	if (*t < EPS)
		return (0);
	(*normal) = cylinder_get_normal(data, ray, *t);
	return (1);
}

void		*cylinder_delete(t_obj3d **obj3d)
{
	ft_memdel(&(*obj3d)->data);
	ft_memdel((void **)obj3d);
	return (NULL);
}

t_obj3d		*cylinder_init(t_point3 center, t_vec3 dir,
							double radius, t_material *mat)
{
	t_obj3d		*obj3d;
	t_cylinder	*p_cylinder;

	if (!(obj3d = ft_memalloc(sizeof(t_obj3d))))
		return (NULL);
	if (!(p_cylinder = ft_memalloc(sizeof(t_cylinder))))
		return (obj3d_delete(&obj3d));
	p_cylinder->pos = center;
	p_cylinder->radius = radius;
	p_cylinder->dir = vec3_normalized(dir);
	obj3d->data = p_cylinder;
	obj3d->type = cylinder;
	obj3d->material = mat;
	obj3d->intersect = &cylinder_intersect;
	obj3d->get_color = &cylinder_get_color;
	return (obj3d);
}
