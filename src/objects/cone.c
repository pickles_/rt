/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cone.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:14:09 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:14:09 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <include/rtv1.h>

#define CONE ((t_cone *)data)
#define COS2 (CONE->cos * CONE->cos)
#define SIN2 (CONE->sin * CONE->sin)

int			cone_get_color(t_obj3d *obj3d)
{
	if (!(obj3d && obj3d->material))
		return (0);
	return (obj3d->material->color | AMASK);
}

t_vec3		cone_get_normal(void *data, t_ray *ray, double t)
{
	t_vec3	p;
	t_vec3	rp;
	t_vec3	n;
	t_vec3	dp;

	p = ray_comp_point(ray, t);
	dp = vec3_sub(p, CONE->pos);
	rp = vec3_sub(dp, vec3_scale(CONE->dir, vec3_dot(CONE->dir, dp)));
	n = vec3_cross(dp, rp);
	n = vec3_sum(n, rp);
	return (vec3_normalized(rp));
}

int			cone_intersect(void *data, t_ray *ray, double *t,
		t_vec3 *normal)
{
	t_vec3		pos;
	t_vec3		u;
	t_vec3		v;
	t_vec3		abc;
	cl_double4	tmp;

	pos = vec3_sub(CONE->pos, ray->orig);
	tmp.s0 = vec3_dot(ray->dir, CONE->dir);
	tmp.s1 = vec3_dot(pos, CONE->dir);
	u = vec3_sub(ray->dir, vec3_scale(CONE->dir, tmp.s0));
	v = vec3_sub(pos, vec3_scale(CONE->dir, tmp.s1));
	abc.s0 = COS2 * vec3_dot(u, u);
	abc.s0 -= SIN2 * tmp.s0 * tmp.s0;
	abc.s1 = 2 * COS2 * vec3_dot(u, v);
	abc.s1 -= 2 * SIN2 * tmp.s0 * tmp.s1;
	abc.s2 = COS2 * vec3_dot(v, v);
	abc.s2 -= SIN2 * tmp.s1 * tmp.s1;
	if (!solve_quadratic(abc.s0, abc.s1, abc.s2, t))
		return (0);
	if (*t < 0.0)
		return (0);
	(*normal) = cone_get_normal(data, ray, *t);
	return (1);
}

void		*cone_delete(t_obj3d **obj3d)
{
	ft_memdel(&(*obj3d)->data);
	ft_memdel((void **)obj3d);
	return (NULL);
}

t_obj3d		*cone_init(t_point3 pos, t_vec3 dir,
		double a, t_material *mat)
{
	t_obj3d		*obj3d;
	t_cone		*p_cone;

	if (!(obj3d = ft_memalloc(sizeof(t_obj3d))))
		return (NULL);
	if (!(p_cone = ft_memalloc(sizeof(t_cone))))
		return (obj3d_delete(&obj3d));
	p_cone->pos = pos;
	p_cone->dir = vec3_normalized(dir);
	p_cone->a = a * M_PI / 180.0;
	p_cone->cos = cos(p_cone->a);
	p_cone->sin = sin(p_cone->a);
	obj3d->data = p_cone;
	obj3d->type = cone;
	obj3d->material = mat;
	obj3d->intersect = &cone_intersect;
	obj3d->get_color = &cone_get_color;
	return (obj3d);
}
