/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   object.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:14:25 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:14:25 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <include/rtv1.h>
#include <libft/includes/libft.h>

void	obj3d_delete_in_list(void *obj3d, size_t unused)
{
	(void)unused;
	obj3d_delete((t_obj3d **)&obj3d);
}

void	*obj3d_delete(t_obj3d **obj3d)
{
	if ((*obj3d)->type == sphere)
		sphere_delete(obj3d);
	else if ((*obj3d)->type == plane)
		plane_delete(obj3d);
	else if ((*obj3d)->type == cone)
		cone_delete(obj3d);
	else if ((*obj3d)->type == cylinder)
		cylinder_delete(obj3d);
	return (NULL);
}
