/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shader.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:14:40 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:14:40 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <include/rtv1.h>

t_vec3	diffuse(t_light *light, t_trace *trace, t_ray *ray)
{
	t_vec3	dintense;
	double	dot;

	dot = vec3_dot(trace->normal, ray->dir);
	dot = ft_clamp(dot, 0.0, 1.0);
	dintense = vec3_product(light->id, trace->obj->material->kd);
	dintense = vec3_scale(dintense, dot);
	return (dintense);
}

t_vec3	specular(t_light *light, t_trace *trace, t_ray *ray)
{
	t_vec3	sintens;
	double	dot;
	t_vec3	v;

	v = vec3_scale(trace->normal, 2 * vec3_dot(ray->dir, trace->normal));
	v = vec3_sub(ray->dir, v);
	dot = vec3_dot(v, trace->ray->dir);
	dot = ft_clamp(dot, 0.0, 1.0);
	dot = dot / vec3_length(v) * vec3_length(trace->ray->dir);
	dot = pow(dot, trace->obj->material->shininess);
	sintens = vec3_scale(light->is, dot);
	return (sintens);
}

t_vec3	phong(t_scene *scene, t_light *light, t_trace *trace)
{
	t_ray	shadowray;
	double	ldist;
	t_obj3d	*inter_obj;
	int		s;
	t_vec3	intense;

	shadowray.orig = vec3_sum(trace->interp,
								vec3_scale(trace->normal, scene->bias));
	shadowray.dir = vec3_sub(light->pos, trace->interp);
	ldist = vec3_normalize(&shadowray.dir);
	inter_obj = comp_intersection(&shadowray, scene->objs, &ldist, &intense);
	s = (inter_obj == NULL);
	intense = vec3_product(light->ia, trace->obj->material->ka);
	intense = vec3_sum(intense,
						vec3_scale(diffuse(light, trace, &shadowray), s));
	intense = vec3_sum(intense,
						vec3_scale(specular(light, trace, &shadowray), s));
	return (intense);
}

int		shader(t_scene *scene, t_trace *trace)
{
	t_light	*light;
	int		lcolor;
	int		rcolor;
	t_vec3	intens;
	t_list	*curr;

	curr = scene->lights;
	rcolor = 0;
	while (curr)
	{
		light = (t_light *)curr->content;
		intens = phong(scene, light, trace);
		lcolor = icolor_blend_add(trace->obj->material->color, light->color);
		lcolor = vec3_color_mult(lcolor, intens);
		rcolor = icolor_blend_add(lcolor, rcolor);
		curr = curr->next;
	}
	return (rcolor);
}
