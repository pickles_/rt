/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 05:54:59 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/22 05:54:59 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <include/rtv1.h>

t_scene	*init_scene(int i)
{
	t_scene	*scene;
	t_scene	*(*init)(void);

	init = NULL;
	if (!(i > 0 && i < 4))
		return (NULL);
	if (i == 1)
		init = &init_scene1;
	else if (i == 2)
		init = &init_scene2;
	else if (i == 3)
		init = &init_scene3;
	if (!init)
		return (NULL);
	if (!(scene = init()))
		return (NULL);
	return (scene);
}
