/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:14:31 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:14:31 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <include/vec3.h>
#include <include/rtv1.h>

t_vec3	ray_comp_point(t_ray *ray, double t)
{
	t_point3	p;
	int			i;

	i = -1;
	while (++i < 3)
		p.s[i] = ray->orig.s[i] + t * ray->dir.s[i];
	return (p);
}
