/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdl2_main_loop.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/12 17:33:08 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/12 17:33:09 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/includes/libft.h"
#include "include/ft_sdl2.h"

void	sdl2_main_loop(t_sdl2 *sdl2)
{
	SDL_Event	e;

	if (!sdl2)
		return ;
	while (1)
	{
		if (SDL_PollEvent(&e))
		{
			if (e.type == SDL_QUIT ||
				(e.type == SDL_KEYUP && e.key.keysym.sym == SDLK_ESCAPE))
				break ;
		}
	}
}
