/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdl2_init.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/12 17:33:04 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/12 17:33:05 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/ft_sdl2.h"
#include "libft/includes/libft.h"

static void	*on_error_log(char *fmt, t_sdl2 **sdl2)
{
	SDL_Log(fmt, SDL_GetError());
	ft_memdel((void **)sdl2);
	return (NULL);
}

t_sdl2		*sdl2_init(void)
{
	t_sdl2			*sdl2;
	SDL_DisplayMode	dmod;

	if (SDL_Init(SDL_INIT_EVERYTHING))
		return (on_error_log("Unable to initialize SDL: %s", NULL));
	if (!(sdl2 = ft_memalloc(sizeof(t_sdl2))))
		return (NULL);
	if (SDL_GetDesktopDisplayMode(0, &dmod))
		return (on_error_log("SDL_GetDesktopDisplayMode failed: %s", &sdl2));
	sdl2->display_mode = dmod;
	sdl2->win = NULL;
	return (sdl2);
}

void		*sdl2_destroy(t_sdl2 **sdl2)
{
	t_win	*next;

	if (!(sdl2 && *sdl2))
		return (NULL);
	while ((*sdl2)->win)
	{
		next = (*sdl2)->win->next;
		sdl2_delete_window(&(*sdl2)->win);
		(*sdl2)->win = next;
	}
	ft_memdel((void **)sdl2);
	return (*sdl2);
}
