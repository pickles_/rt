/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdl2_put_image_to_window.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/12 17:33:12 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/12 17:33:13 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/ft_sdl2.h"

void	sdl2_put_image_to_window(t_win *win, t_img *img, int x, int y)
{
	SDL_Rect	dstrect;

	dstrect.x = x;
	dstrect.y = y;
	dstrect.w = img->width;
	dstrect.h = img->height;
	SDL_UpdateTexture(img->texture, NULL, img->pixels,
						img->surface->pitch);
	SDL_RenderClear(win->renderer);
	SDL_RenderCopy(win->renderer, img->texture, NULL, &dstrect);
	SDL_RenderPresent(win->renderer);
}
