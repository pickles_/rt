/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:14:14 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:14:15 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <include/rtv1.h>
#include <libft/includes/libft.h>

void	*env_delete(t_env **env)
{
	sdl2_destroy_img(&(*env)->img);
	sdl2_destroy_window((*env)->sdl2, (*env)->win);
	sdl2_destroy(&(*env)->sdl2);
	ft_memdel((void **)env);
	return (NULL);
}

t_env	*env_init(void)
{
	t_env	*env;

	if (!(env = ft_memalloc(sizeof(t_env))))
		return (NULL);
	if (!(env->sdl2 = sdl2_init()))
		return (env_delete(&env));
	if (!(env->win = sdl2_new_win(env->sdl2, "RTv1", WINW, WINH)))
		return (env_delete(&env));
	if (!(env->img = sdl2_new_img(env->win, WINW, WINH)))
		return (env_delete(&env));
	return (env);
}
