/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec3.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:14:55 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:15:00 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <libft/includes/libft.h>
#include "include/vec3.h"

t_vec3	vec3_new(double x, double y, double z)
{
	t_vec3	v;

	v.x = x;
	v.y = y;
	v.z = z;
	return (v);
}

double	vec3_length2(t_vec3 v)
{
	return (vec3_dot(v, v));
}

double	vec3_length(t_vec3 v)
{
	return (sqrt(vec3_length2(v)));
}

t_vec3	vec3_normalized(t_vec3 v)
{
	t_vec3	ret;
	double	l;

	l = vec3_length(v);
	ft_bzero(ret.s, sizeof(cl_double) * 3);
	if (l != 0.0)
	{
		ret.x = v.x / l;
		ret.y = v.y / l;
		ret.z = v.z / l;
	}
	return (ret);
}

double	vec3_normalize(t_vec3 *v)
{
	double len;

	len = vec3_length(*v);
	(*v) = vec3_normalized(*v);
	return (len);
}
