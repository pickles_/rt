/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_abs.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/09 23:16:33 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/09 23:16:37 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned long long int	ft_llabs(long long int nb)
{
	return ((unsigned long long int)((nb < 0) ? -nb : nb));
}

unsigned long int		ft_labs(long int nb)
{
	return ((unsigned long)(ft_llabs((long long int)nb)));
}

unsigned int			ft_abs(int nb)
{
	return ((int)(ft_llabs((long long int)nb)));
}

double					ft_fabs(double nb)
{
	return ((nb < 0) ? -nb : nb);
}
