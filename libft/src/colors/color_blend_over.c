/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_blend_over.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 17:45:33 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/26 17:46:19 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_colors.h>

t_color				color_blend_over(t_color a, t_color b)
{
	t_color			c;

	if (b.argb.a == 0)
		return (a);
	else if (b.argb.a == 255 || a.argb.a == 0)
		return (b);
	else if (a.argb.a < 255)
	{
		c.argb.a = a.argb.a + ((255 - a.argb.a) * b.argb.a) / 255;
		c.argb.r = (a.argb.r * a.argb.a * (255 - b.argb.a) +
			b.argb.r * b.argb.a) / c.argb.a;
		c.argb.g = (a.argb.g * a.argb.a * (255 - b.argb.a) +
			b.argb.g * b.argb.a) / c.argb.a;
		c.argb.b = (a.argb.b * a.argb.a * (255 - b.argb.a) +
			b.argb.b * b.argb.a) / c.argb.a;
	}
	else
	{
		c.argb.a = 255;
		c.argb.r = (a.argb.r * (255 - b.argb.a) + b.argb.r * b.argb.a) / 255;
		c.argb.g = (a.argb.g * (255 - b.argb.a) + b.argb.g * b.argb.a) / 255;
		c.argb.b = (a.argb.b * (255 - b.argb.a) + b.argb.b * b.argb.a) / 255;
	}
	return (c);
}
