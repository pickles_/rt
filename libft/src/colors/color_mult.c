/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_mult.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:23:44 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:23:47 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_color		color_mult(t_color a, t_color b)
{
	t_color	c;

	c.argb.r = a.argb.r * b.argb.r;
	c.argb.g = a.argb.g * b.argb.g;
	c.argb.b = a.argb.b * b.argb.b;
	return (c);
}

int			icolor_mult(int a, int b)
{
	return (color_mult((t_color)a, (t_color)b).color);
}
