/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 17:45:15 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/26 17:48:05 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_lists.h>

void	ft_lstrev(t_list **alst)
{
	t_list	*new;
	t_list	*old_next;
	t_list	*old_current;

	new = 0;
	old_current = *alst;
	while (old_current)
	{
		old_next = old_current->next;
		old_current->next = new;
		new = old_current;
		old_current = old_next;
	}
	*alst = new;
}
